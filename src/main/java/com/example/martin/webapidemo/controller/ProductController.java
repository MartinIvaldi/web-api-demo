package com.example.martin.webapidemo.controller;

import com.example.martin.webapidemo.service.dto.ProductCreateUpdateDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public interface ProductController {
    @GetMapping()
    ResponseEntity all();

    @PostMapping()
    ResponseEntity create(@RequestBody ProductCreateUpdateDto product);

    @GetMapping("{id}")
    ResponseEntity get(@PathVariable Long id);

    @PutMapping("{id}")
    ResponseEntity update(@PathVariable Long id, @RequestBody ProductCreateUpdateDto product);

    @DeleteMapping("/{id}")
    ResponseEntity delete(@PathVariable Long id);
}

package com.example.martin.webapidemo.controller.impl;

import com.example.martin.webapidemo.controller.CategoryController;
import com.example.martin.webapidemo.service.CategoryService;
import com.example.martin.webapidemo.service.dto.CategoryCreateUpdateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("categories")
public class CategoryControllerImpl implements CategoryController {

    @Autowired
    private CategoryService categoryService;

    @Override
    @GetMapping("")
    public ResponseEntity all() {
        return ResponseEntity.ok(categoryService.all());
    }

    @Override
    @PostMapping("")
    public ResponseEntity create(@RequestBody CategoryCreateUpdateDto category) {
        return new ResponseEntity(categoryService.create(category), HttpStatus.CREATED);
    }

    @Override
    @GetMapping("/{id}")
    public ResponseEntity get(@PathVariable Long id) {
        return ResponseEntity.ok(categoryService.get(id));
    }

    @Override
    @PutMapping("/{id}")
    public ResponseEntity update(@PathVariable Long id, @RequestBody CategoryCreateUpdateDto category) {
        categoryService.update(id, category);
        return ResponseEntity.noContent().build();
    }

    @Override
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        categoryService.delete(id);
        return ResponseEntity.noContent().build();
    }
}

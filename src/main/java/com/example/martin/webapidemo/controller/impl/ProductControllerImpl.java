package com.example.martin.webapidemo.controller.impl;

import com.example.martin.webapidemo.controller.ProductController;
import com.example.martin.webapidemo.service.ProductService;
import com.example.martin.webapidemo.service.dto.ProductCreateUpdateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("products")
public class ProductControllerImpl implements ProductController {

    @Autowired
    private ProductService productService;

    @Override
    @GetMapping()
    public ResponseEntity all() {
        return ResponseEntity.ok(productService.all());
    }

    @Override
    @PostMapping()
    public ResponseEntity create(@RequestBody ProductCreateUpdateDto product) {
        return new ResponseEntity(productService.create(product), HttpStatus.CREATED);
    }

    @Override
    @GetMapping("/{id}")
    public ResponseEntity get(@PathVariable Long id) {
        return ResponseEntity.ok(productService.get(id));
    }

    @Override
    @PutMapping("/{id}")
    public ResponseEntity update(@PathVariable Long id, @RequestBody ProductCreateUpdateDto product) {
        productService.update(id, product);
        return ResponseEntity.noContent().build();
    }

    @Override
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        productService.delete(id);
        return ResponseEntity.noContent().build();
    }
}

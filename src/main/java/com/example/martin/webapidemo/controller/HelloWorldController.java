package com.example.martin.webapidemo.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloWorldController {

    @RequestMapping("/asdas")
    public String index() {
        return "Greetings from Spring Boot!";
    }

}

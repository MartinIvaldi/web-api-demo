package com.example.martin.webapidemo.controller.handler;

import com.example.martin.webapidemo.service.exception.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Date;

@RestControllerAdvice
public class RestExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(RestExceptionHandler.class);

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    ErrorDto employeeNotFoundHandler(ResourceNotFoundException ex) {
        return  new ErrorDto(new Date(), ex.getMessage());
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    ErrorDto genericErrorHadler(Exception ex) {
        log.error(ex.getMessage(),ex );
        return new ErrorDto(new Date(), "An internal error ocurred");
    }

    class ErrorDto{

        private Date timestamp;
        private String message;

        public ErrorDto(Date timestamp, String message) {
            this.timestamp = timestamp;
            this.message = message;
        }
        public Date getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(Date timestamp) {
            this.timestamp = timestamp;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        @Override
        public String toString() {
            return "ErrorDto{" +
                    "timestamp=" + timestamp +
                    ", message='" + message + '\'' +
                    '}';
        }
    }
}






//{
//        "timestamp": "2021-07-09T17:23:05.111+00:00",
//        "status": 500,
//        "error": "Internal Server Error",
//        "path": "/products"
//        }
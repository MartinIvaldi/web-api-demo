package com.example.martin.webapidemo.controller;

import com.example.martin.webapidemo.service.dto.CategoryCreateUpdateDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public interface CategoryController {
    @GetMapping()
    ResponseEntity all();

    @PostMapping()
    ResponseEntity create(@RequestBody CategoryCreateUpdateDto category);

    @GetMapping("/{id}")
    ResponseEntity get(@PathVariable Long id);

    @PutMapping("/{id}")
    ResponseEntity update(@PathVariable Long id, @RequestBody CategoryCreateUpdateDto category);

    @DeleteMapping("/{id}")
    ResponseEntity delete(@PathVariable Long id);
}

package com.example.martin.webapidemo.service.impl;

import com.example.martin.webapidemo.data.entity.Product;
import com.example.martin.webapidemo.data.repository.ProductRepository;
import com.example.martin.webapidemo.service.ProductService;
import com.example.martin.webapidemo.service.dto.ProductCreateUpdateDto;
import com.example.martin.webapidemo.service.dto.ProductGetDto;
import com.example.martin.webapidemo.service.exception.ResourceNotFoundException;
import com.example.martin.webapidemo.service.mapper.ProductMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private static final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductMapper productMapper;

    @Override
    public List<ProductGetDto> all() {
        log.info("Searching all products");
        List<ProductGetDto> products = new ArrayList<ProductGetDto>();
        productRepository.findAll().forEach(product -> products.add(productMapper.mapToDto(product)));
        return products;
    }

    @Override
    public Long create(ProductCreateUpdateDto productDto) {
        log.info("Creating new product: " + productDto);
        return productRepository.save(productMapper.mapToEntity(productDto)).getId();
    }

    @Override
    public ProductGetDto get(Long id) {
        log.info("Obtaining product with id: " + id);
        ProductGetDto product = productMapper.mapToDto(productRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("product", id)));
        return product;
    }

    @Override
    public void update(Long id, ProductCreateUpdateDto productDto) {
        log.info("Updating product with id: " + id + " with te data: "+ productDto);
        Product product = productRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("product", id));
        Product productToUpdate= productMapper.mapToEntity(productDto);
        productToUpdate.setId(product.getId());
        productRepository.save(productToUpdate);
    }

    @Override
    public void delete(Long id) {
        log.info("Deleting category with id: " + id );
        productRepository.deleteById(id);
    }
}

package com.example.martin.webapidemo.service;

import com.example.martin.webapidemo.service.dto.CategoryCreateUpdateDto;
import com.example.martin.webapidemo.service.dto.CategoryGetDto;

import java.util.List;

public interface CategoryService {
    List<CategoryGetDto> all();

    Long create(CategoryCreateUpdateDto categoryDto);

    CategoryGetDto get(Long id);

    void update(Long id, CategoryCreateUpdateDto categoryDto);

    void delete(Long id);
}

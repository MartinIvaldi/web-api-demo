package com.example.martin.webapidemo.service;

import com.example.martin.webapidemo.service.dto.ProductCreateUpdateDto;
import com.example.martin.webapidemo.service.dto.ProductGetDto;

import java.util.List;

public interface ProductService {
    List<ProductGetDto> all();

    Long create(ProductCreateUpdateDto productDto);

    ProductGetDto get(Long id);

    void update(Long id, ProductCreateUpdateDto productDto);

    void delete(Long id);
}

package com.example.martin.webapidemo.service.mapper;

import com.example.martin.webapidemo.data.entity.Category;
import com.example.martin.webapidemo.service.dto.CategoryGetDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CategoryMapper {
    CategoryGetDto mapToDto(Category source);
}

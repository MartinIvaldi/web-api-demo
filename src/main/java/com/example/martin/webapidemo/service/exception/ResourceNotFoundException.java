package com.example.martin.webapidemo.service.exception;

public class ResourceNotFoundException extends RuntimeException {

    public ResourceNotFoundException(String resourceName, Long id) {
        super("Could not find "+ resourceName +" " + id);
    }
}

package com.example.martin.webapidemo.service.impl;

import com.example.martin.webapidemo.data.entity.Category;
import com.example.martin.webapidemo.data.repository.CategoryRepository;
import com.example.martin.webapidemo.service.CategoryService;
import com.example.martin.webapidemo.service.dto.CategoryCreateUpdateDto;
import com.example.martin.webapidemo.service.dto.CategoryGetDto;
import com.example.martin.webapidemo.service.exception.ResourceNotFoundException;
import com.example.martin.webapidemo.service.mapper.CategoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private static final Logger log = LoggerFactory.getLogger(CategoryServiceImpl.class);

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public List<CategoryGetDto> all() {
        log.info("Searching all categories");
        List<CategoryGetDto> categories = new ArrayList<CategoryGetDto>();
        categoryRepository.findAll().forEach(category -> categories.add(categoryMapper.mapToDto(category)));
        return categories;
    }

    @Override
    public Long create(CategoryCreateUpdateDto categoryDto) {
        log.info("Creating new category: " + categoryDto);
        return categoryRepository.save(new Category(categoryDto.getName())).getId();
    }

    @Override
    public CategoryGetDto get(Long id) {
        log.info("Obtaining category with id: " + id);
        CategoryGetDto category = categoryMapper.mapToDto(categoryRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("category", id)));
        return category;
    }

    @Override
    public void update(Long id, CategoryCreateUpdateDto categoryDto) {
        log.info("Updating category with id: " + id + " with te data: "+ categoryDto);
        Category category = categoryRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("category", id));
        category.setName(categoryDto.getName());
        categoryRepository.save(category);
    }

    @Override
    public void delete(Long id) {
        log.info("Deleting category with id: " + id );
        categoryRepository.deleteById(id);
    }
}

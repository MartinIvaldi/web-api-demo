package com.example.martin.webapidemo.service.mapper;

import com.example.martin.webapidemo.data.entity.Product;
import com.example.martin.webapidemo.service.dto.ProductGetDto;
import com.example.martin.webapidemo.service.dto.ProductCreateUpdateDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface ProductMapper {
    ProductGetDto mapToDto(Product product);

    @Mappings({
            @Mapping(target="category.id", source="productDto.categoryId")
    })
    Product mapToEntity(ProductCreateUpdateDto productDto);
}

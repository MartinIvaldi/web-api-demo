package com.example.martin.webapidemo.service.dto;

public class CategoryCreateUpdateDto {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CategoryCreateUpdateDto{" +
                "name='" + name + '\'' +
                '}';
    }
}

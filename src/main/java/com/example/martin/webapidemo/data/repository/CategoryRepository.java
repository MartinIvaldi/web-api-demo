package com.example.martin.webapidemo.data.repository;

import com.example.martin.webapidemo.data.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {
}

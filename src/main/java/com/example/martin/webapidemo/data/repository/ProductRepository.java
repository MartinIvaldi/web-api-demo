package com.example.martin.webapidemo.data.repository;

import com.example.martin.webapidemo.data.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
}

package com.example.martin.webapidemo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.zalando.logbook.DefaultHttpLogWriter;
import org.zalando.logbook.DefaultSink;
import org.zalando.logbook.Logbook;
import org.zalando.logbook.SplunkHttpLogFormatter;


@Configuration
class Config {

    @Bean
    public Logbook logbook(){
        return Logbook.builder()
                .sink(new DefaultSink(
                        new SplunkHttpLogFormatter(),
                        new DefaultHttpLogWriter())
                ).build();
    }

}